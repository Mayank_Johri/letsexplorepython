
# Introduction

This is my attempt to consolidate my training materials on Functional Programming in Python. Once consolidated, it will become part of my upcoming book "_Lets Explore: Advance Python_" which will contains all the remaining topics left from Core Python.

Future Plans: Plan is to an e.book on Advance topics on Python and this book will be a stepping stone for that in mind.

As always, for any changes, correction, suggestions etc, I can be reached at mayankjohri@gmail.com.
